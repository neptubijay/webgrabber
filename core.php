
<?php 
ini_set('max_execution_time', 360);
	/* <<<<  >>>>*/ 
	require'simple_html_dom.php';		//PHP Simple HTML DOM Parser
?>			

<?php
		error_reporting(0);
		//error_reporting(E_ALL);
		$srcCode = $_POST['doc'];		//whole html file 

		$url = $_POST['Url'];				//site URL
		$url_path = $url; 

		// $preg = array('/',':','.','?','&','=','_');
		// $name=str_replace($preg, '', $url_path);
		
		$var = parse_url($url_path, PHP_URL_HOST);
		$name=str_replace('.', '', $var);

		if(!file_exists('template/'.$name))	//template folder
			{
				mkdir('template/'.$name,0777, true);
			}

		
			$create=fopen('template/'.$name.'/'.'index.html','w+') or die("can't open file");
			$write=fwrite($create,$srcCode);
			fclose($create);


			$dom = new simple_html_dom();
			$html = file_get_html('template/'.$name.'/'.'index.html');
			

			function is_Valid_URL($urli)
			{
			  return preg_match('%^((https?://)|(www\.))([a-z0-9-].?)+(:[0-9]+)?(/.*)?$%i', $urli);
			}


			foreach($html as $content )
			{
					$content = preg_replace( '/<!--(.|\s)*?-->/' , '' , $content );		//remove comment 
			}

			$data = $html;
			$handle=fopen('template/'.$name.'/'.'index.html','w+') or die("can't open file");
			fwrite($handle,$data);
			fclose($handle);

			foreach($html->find('a') as $href) 	
				{ 

					$href->href="javascript:void(0)";		//change all links href to "javascript:void(0)"	
		        }

		    $doc = $html;
		    $read=fopen('template/'.$name.'/'.'index.html','w+') or die("can't open file");
			$modify=fwrite($read,$doc);
			fclose($read);

			foreach($html->find('script[src=../uxcam/ux.js]') as $data )
			{
					$data->src=null;				//recorder.js will be hidden
			}

			$data = $html;
			$handle=fopen('template/'.$name.'/'.'index.html','w+') or die("can't open file");
			fwrite($handle,$data);
			fclose($handle);
		
			foreach($html->find('script') as $script)		//get script 
					{						
						$scriptPath=$script->src;
						$path="";
						$js = explode("/", $scriptPath);
						$jsName = end($js);
						$urlvalid = is_Valid_URL($scriptPath);
						if(!$urlvalid)
						{
			  				for ($i=0;$i<(count($js)-1);$i++) 
							 	{
							        $path .= $js[$i] . "/";
							        if(!file_exists('template/'.$name.'/'.$path))
									{
										mkdir('template/'.$name.'/'.$path, 0777, true);		//js folder created
									}
								}
								if(!file_exists('template/'.$name.'/'.$path.$jsName))
								{
									file_put_contents('template/'.$name.'/'.$path.$jsName,file_get_contents($url_path.$scriptPath));
								}
						}
					}
					foreach($html->find('input') as $input) 
					{
						$inputsrc=$input->src;
						$path="";
					 	$explode= explode("/", $inputsrc);
					 	$inputname = end($explode);

					 	$urlvalid = is_Valid_URL($inputsrc);
						if(!$urlvalid)
						{
							for ($i=0;$i<(count($explode)-1);$i++) 
						 	  {
						        $path .= $explode[$i] . "/";
						        if(!file_exists('template/'.$name.'/'.$path))
									{
										mkdir('template/'.$name.'/'.$path, 0777, true);		//img folder created
									}		
						      }
						
						
							if(!file_exists('template/'.$name.'/'.$path.$inputname))
								{
									file_put_contents('template/'.$name.'/'.$path.$inputname,file_get_contents($url_path.$inputsrc)); 	//img downloaded
								}
						}
					}
			foreach($html->find('img') as $img) 	//image
					{
						$imgpath=$img->src;
						$path="";
					 	$image = explode("/", $imgpath);
					 	$imgName = end($image);

					 	$urlvalid = is_Valid_URL($scriptPath);
						if(!$urlvalid)
						{
							for ($i=0;$i<(count($image)-1);$i++) 
						 	  {
						        $path .= $image[$i] . "/";
						        if(!file_exists('template/'.$name.'/'.$path))
									{
										mkdir('template/'.$name.'/'.$path, 0777, true);		//img folder created
									}		
						      }
						
						
							if(!file_exists('template/'.$name.'/'.$path.$imgName))
								{
									file_put_contents('template/'.$name.'/'.$path.$imgName,file_get_contents($url_path.$imgpath)); 	//img downloaded
								}
						}
					} 

				function extract_css_urls( $text )
					{
						$urls = array( );
						$url_pattern     = '(([^\\\\\'", \(\)]*(\\\\.)?)+)';
						$urlfunc_pattern = 'url\(\s*[\'"]?' . $url_pattern . '[\'"]?\s*\)';
						$pattern         = '/(' .
							 '(@import\s*[\'"]' . $url_pattern     . '[\'"])' .
							'|(@import\s*'      . $urlfunc_pattern . ')'      .
							'|('                . $urlfunc_pattern . ')'      .  ')/iu';
						if ( !preg_match_all( $pattern, $text, $matches ) )
							return $urls;
						foreach ( $matches[3] as $match )
							if ( !empty($match) )
								$urls['import'][] = 
									preg_replace( '/\\\\(.)/u', '\\1', $match );
						foreach ( $matches[7] as $match )
							if ( !empty($match) )
								$urls['import'][] = 
									preg_replace( '/\\\\(.)/u', '\\1', $match );
						foreach ( $matches[11] as $match )
							if ( !empty($match) )
								$urls['property'][] = 
									preg_replace( '/\\\\(.)/u', '\\1', $match );
						return $urls;
					}

			foreach($html->find('link') as $link)	//get link 
					{
							if(strtolower($link->getAttribute('rel')) == "stylesheet" ) 
							{
							    $linkpath=$link->getAttribute('href');
						    }
								$links = explode("/", $linkpath);
								$path="";	
								$linkName = end($links);
								$urlvalid = is_Valid_URL($linkpath);
								if(!$urlvalid)
								{
									for ($i=0;$i<(count($links)-1);$i++) 
							 	  	{
								        $path .= $links[$i] . "/";
								        if(!file_exists('template/'.$name.'/'.$path))					//css folder
											{
												mkdir('template/'.$name.'/'.$path, 0777, true);		
											}		
							      	}
							      	if(!file_exists('template/'.$name.'/'.$path.$linkName))
							      	{
										file_put_contents('template/'.$name.'/'.$path.$linkName,file_get_contents($url_path.$linkpath));	//download css
									}
								}

							$url='template/'.$name.'/'.$path.$linkName;
							$text = file_get_contents( $url );
							$urls = extract_css_urls( $text );

							if ( !empty( $urls['import'] ) )
							{
								for($i=0;$i<=count($urls['import']);$i++)
								{
									$u=$urls['import'][$i];
									if(!file_exists('template/'.$name.'/'.$u))
					      			{
										file_put_contents('template/'.$name.'/'.$u,file_get_contents($url_path.$u));
									}
								}
							}

							if ( !empty( $urls['property'] ) )
							{
								for($i=0;$i<=count($urls['property']);$i++)
								{
									$p=$urls['property'][$i];
									if(!file_exists('template/'.$name.'/'.$p))
					      			{
										file_put_contents('template/'.$name.'/'.$p,file_get_contents($url_path.$p));
									}
								}
							}
					}

			$exit=$html->clear();
			unset($exit);
?>